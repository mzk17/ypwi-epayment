<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Events\ActivePaymentCreated;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class LinkActivePaymentToStudentTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $payload = ['activePaymentCreated' => true];
        $response = $this->json('POST', '/api/v1/epy/test/link-active-payment-to-students', $payload);

        $response->assertStatus(200);
    }
}
