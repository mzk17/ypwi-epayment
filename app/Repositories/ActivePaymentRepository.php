<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\ActivePayment;

class ActivePaymentRepository extends ActivePayment
{
    public function model ()
    {
        return 'App\Models\ActivePayment';
    }
}
