<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\StudentPaymentItem;

class StudentPaymentItemRepository extends StudentPaymentItem
{
    public function model ()
    {
        return 'App\Models\StudentPaymentItem';
    }
}
