<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->string('account_number');
            $table->string('account_type');
            $table->string('account_name');
            $table->string('bank_id');
            $table->string('school_id');
            $table->string('school_class_flag')->nullable(); // Special binding to specific class
            $table->string('payment_flag')->nullable(); // Special binding to specific payment
            $table->timestamps();

            $table->primary('account_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
