<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentItemBaseCost extends Model
{
    public function paymentItem ()
    {
        return $this->belongsTo('App\Models\PaymentItem', 'payment_item_id');
    }
}
