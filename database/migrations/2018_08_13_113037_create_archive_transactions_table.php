<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchiveTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archive_transactions', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('nis');
            $table->string('student_name');
            $table->string('payment_name');
            $table->double('transaction_amount', 18, 2);
            $table->longText('transaction_note')->nullable();
            $table->timestamp('transaction_date');
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archive_transactions');
    }
}
