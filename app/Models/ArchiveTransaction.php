<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArchiveTransaction extends Model
{
    public $incrementing = false;
    protected $guarded   = [];
}
