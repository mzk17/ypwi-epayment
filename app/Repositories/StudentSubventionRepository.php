<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\StudentSubvention;

class StudentSubventionRepository extends StudentSubvention
{
    public function model ()
    {
        return 'App\Models\StudentSubvention';
    }
}
