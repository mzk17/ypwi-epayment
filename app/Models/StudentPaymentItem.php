<?php

/**
 * Description
 * activity_status should have 2 options
 * 1 => Show on app
 * 2 => Disabled. Not show and noted/calculate for graduation on app
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentPaymentItem extends Model
{
    public $incrementing = false;
    protected $guarded = [];

    public function activePayment ()
    {
        return $this->belongsTo('App\Models\ActivePayment', 'active_payment_id');
    }

    public function transactions ()
    {
        return $this->hasMany('App\Models\StudentPaymentTransaction', 'student_payment_id');
    }
}
