<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\StudentPaymentTransaction;

class StudentPaymentTransactionRepository extends StudentPaymentTransaction
{
    public function model ()
    {
        return 'App\Models\StudentPaymentTransaction';
    }
}
