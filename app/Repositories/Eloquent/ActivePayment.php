<?php

namespace App\Repositories\Eloquent;

use App\Helpers\ActivePayment\ActivePaymentGenerator;

abstract class ActivePayment extends ActivePaymentGenerator
{
    /**
     * List of All Active Payments
     */
    public function index ()
    {
        try {
            return $this->model->where('status', true)->get();
        }
        catch(Exception $e) {
            throw new Error($e->getMessage());
        }
    }
    /**
     * Main function
     */
    public function generate ($payload)
    {
        if ($payload->education_period_created) {

            // Deactivate previous active payments
            $this->deactivatePreviousPayments();

            /**
              * Define total active payments based on its timing accordingly
              * and create it
              */
            return $this->createActivePayments($payload);

        }
    }

    public function activation ($payload)
    {
        if ($payload->agree_to_change_activity_status) {

            try {
                $activePayment = $this->model->find($payload->active_payment_id);
                // Update self table
                $this->model->where('id', $payload->active_payment_id)
                    ->update(['status' => $payload->status]);

                // Update student payment items table
                \App\Models\StudentPaymentItem::where('active_payment_id', $payload->active_payment_id)
                    ->update(['activity_status' => $payload->status]);

                $message = ($payload->status == 1) ? "Status item pembayaran {$activePayment->active_payment_name} berhasil diaktifkan kembali" : "Status item pembayaran {$activePayment->active_payment_name} berhasil di-nonaktifkan";
                return $message;
            }
            catch (Exception $e) {
                throw new Error($e->getMessage());
            }
        }
    }

    public function epCanceled ($payload)
    {
        // Take all currently new Active Payments (APs)
        // to cancel it later
        $activePayments = $this->model->where('status', 1)
            ->select('id')
            ->pluck('id');

        // Take all StudentPayment that bind to those APs
        $stps = \App\Models\StudentPaymentItem::whereIn('active_payment_id', $activePayments)->select('id')->pluck('id');

        try {
            // Delete PI Base Costs that bind to those APs
            \App\Models\PaymentItemBaseCost::where('period_id', $payload->false_ep)->delete();

            // Delete StudentPayment Items thta bind to those APs
            if (count($stps) > 0)
                \App\Models\StudentPaymentItem::destroy($stps);

            // Delete the APs
            $this->model->whereIn('id', $activePayments)->delete();

            // Reactivated all previous APs
            $this->reactivatedPreviousAP($payload->prev_ep);

            return "Item pembiayaan pada Tahun Pelajaran {$payload->false_ep} berhasil dibatalkan";
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    private function reactivatedPreviousAP ($previousEP)
    {
        try {
            $this->model->where("active_payment_name", "LIKE", "%{$previousEP}%")
                ->update(["status" => 1]);

            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }
}
