<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveStatusAndModifiedExistingStatusOnStudentPaymentItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_payment_items', function (Blueprint $table) {
            $table->renameColumn('status', 'transaction_status');
            $table->boolean('activity_status')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_payment_items', function (Blueprint $table) {
            $table->renameColumn('transaction_status', 'status');
            $table->dropColumn('activity_status');
        });
    }
}
