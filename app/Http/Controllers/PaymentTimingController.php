<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PaymentTimingRepository as PaymentTiming;

class PaymentTimingController extends Controller
{
    private $paymentTiming;

    public function __construct (PaymentTiming $paymentTiming)
    {
        $this->paymentTiming = $paymentTiming;
    }

    public function index ()
    {
        return response()->json(
            $this->paymentTiming->all()
        );
    }

    public function store (Request $request)
    {
        return response()->json(
            $this->paymentTiming->store($request)
        );
    }

    public function update ($timingId, Request $request)
    {
        return response()->json(
            $this->paymentTiming->edit($timingId, $request)
        );
    }

    public function destroy ($timingId)
    {
        return response()->json(
            $this->paymentTiming->delete($timingId)
        );
    }
}
