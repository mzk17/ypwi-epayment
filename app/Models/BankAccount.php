<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    public $incrementing = false;
    protected $guarded = [];
    protected $primaryKey = 'account_number';

    public function bankProfile ()
    {
        return $this->belongsTo('App\Models\BankCollaborator', 'bank_id');
    }
}
