<?php

namespace App\Repositories\Eloquent;

use Uuid;
use App\Models\PaymentItem;
use App\Models\ActivePayment;
use App\Models\StudentPaymentItem;
use App\Models\PaymentItemBaseCost;
use App\Repositories\Eloquent\GenericRepository;

abstract class StudentSubvention extends GenericRepository
{
    public function index ($nis)
    {
        return $this->model->where('nis', $nis)->get();
    }
    
    public function available ($nis)
    {
        // Get subvented payment of a Student
        $exists = $this->model->where('nis', $nis)->pluck('payment_item_id');

        return PaymentItem::select('id', 'payment_item_name')
            ->whereNotIn('id', $exists)
            ->get();
    }

    public function store ($payload)
    {
        $generatedId = Uuid::generate(4);

        try {
            // Store it first
            $this->model->create(array_merge(
                    ['id' => $generatedId],
                    $payload->all()
                )
            );

            $this->updateStudentPayment(
                $payload->nis,
                $payload->payment_item_id,
                $payload->subvention_cost
            );

            return "Subsidi berhasil diberlakukan";
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    public function edit ($payload)
    {
        $subvention = $this->model->find($payload->id);
        $nis = $subvention->nis;
        $paymentItemId = $subvention->payment_item_id;

        try {
            $subvention->subvention_cost = $payload->updated_cost;

            $subvention->save();

            $this->updateStudentPayment($nis, $paymentItemId, $payload->updated_cost);

            return "Subsidi berhasil diupdate";
        }
        catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function cancel ($payload)
    {
        // Student
        $subvention = $this->model->find($payload->id);

        $activePayments = $this->getActivePayments($subvention->payment_item_id);
        $baseCost = PaymentItemBaseCost::where('payment_item_id',$subvention->payment_item_id)
            ->where('school_id', $payload->school_id)
            ->where('period_id', $payload->period_id)
            ->get()[0];

        try {
            $subvention->delete();

            // Update he/she's payment to base cost
            StudentPaymentItem::where('nis', $payload->nis)
                ->whereIn('active_payment_id', $activePayments)
                ->update(['cost' => $baseCost->cost]);

            return "Subsidi pembiayaan berhasil dibatalkan";
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    private function getActivePayments ($paymentItemId)
    {
        return ActivePayment::where('payment_item_id', $paymentItemId)
            ->where('status', 1)
            ->pluck('id');
    }

    private function updateStudentPayment ($nis, $paymentItemId, $subventionCost)
    {
        $activePayments = $this->getActivePayments($paymentItemId);
        $updatePayload  = ['cost' => $subventionCost];

        if ($subventionCost == 0) {
            array_merge($updatePayload, ['transaction_status' => 1]);
        }

        // Update StudentPaymentItem
        StudentPaymentItem::where('nis', $nis)
            ->whereIn('active_payment_id', $activePayments)
            ->update($updatePayload);
    }
}