<?php

namespace App\Helpers\Transaction;

use App\Models\StudentPaymentItem;
use App\Models\StudentPaymentTransaction;

trait CalculateRemainingCost
{
    public function remainingCost ($studentPaymentId)
    {
        // Take the initial Cost of the payment item
        $studentPaymentItem = StudentPaymentItem::find($studentPaymentId);

        // Fetch all transactions to that item and sum it
        $sumTransactions = StudentPaymentTransaction::where('student_payment_id', $studentPaymentId)->sum('checkout');

        // Substract the initial cost and sum of transactons
        $remainder = $studentPaymentItem->cost - $sumTransactions;

        // If step above failed, then it may multiple payments at once transaction
        if ($sumTransactions == 0) {
            $remainder = $this->multiplePayments($studentPaymentId);
        }

        // Return the remainder
        return $remainder;
    }

    private function multiplePayments ($studentPaymentId)
    {
        $exist = StudentPaymentTransaction::where('student_payment_id', 'like', "%$studentPaymentId%")->get();

        if (!$exist->isEmpty()) {
            $remainder = $exist[0]->checkout;
            $payments = explode(';', $exist[0]->student_payment_id);
            
            foreach ($payments as $key => $studentPaymentId) {
                $studentPaymentItem = StudentPaymentItem::find($studentPaymentId);
                $remainder = $remainder - $studentPaymentItem->cost;
            }

            return $remainder;
        }
    }
}