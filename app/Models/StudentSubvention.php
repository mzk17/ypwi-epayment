<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentSubvention extends Model
{
    public $incrementing = false;
    protected $guarded = [];

    public function paymentItem ()
    {
        return $this->belongsTo('App\Models\PaymentItem', 'payment_item_id');
    }
}
