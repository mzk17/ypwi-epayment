<?php

namespace App\Listeners;

use Ixudra\Curl\Facades\Curl;
use App\Models\PaymentItem;
use App\Helpers\CurlVariables;
use App\Events\NewEducationPeriodActivated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use DB;

class CreatePaymentItemBaseCost implements ShouldQueue
{
    use CurlVariables;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle()
    {
        // Init
        $payload = [];
        $schools = DB::connection('ypwi_bank_data')->table('schools')->select('id')->pluck('id');

        $paymentItems = PaymentItem::select('id')->get();
        $epActive = DB::connection('ypwi_miss')
            ->table("education_periods")
            ->select("id")
            ->where("status", 1)
            ->get()[0];

        foreach ($schools as $key => $schoolId) {
            foreach ($paymentItems as $piKey => $paymentItem) {
                array_push($payload, [
                    'payment_item_id' => $paymentItem->id,
                    'school_id' => $schoolId,
                    'period_id' => $epActive->id,
                    'cost' => 0
                ]);
            }
        }

        DB::table('payment_item_base_costs')->insert($payload);
    }

    public function failed (PaymentItemCreated $event, $exception)
    {
        $message = "Event failed with this message: " . $exception;
        return response()->json($message);
    }
}
