<?php

namespace App\Helpers\ActivePayment;

use Jenssegers\Date\Date;

trait OccurenceAndNameGenerator
{
    /**
     *  @var reqStartTime
     *  Request start time of a payment.
     *  Value in month
     */
    public function generator ($index, $paymentItem, $reqStartTime = null, $epActive = null)
    {
        if (!is_null($reqStartTime) && !is_null($epActive)) {
            $monthFreq  = $paymentItem->timing_amount;
            $parsedDate = Date::parse(str_replace('.', '-', $reqStartTime) . '-01');

            if ($index > 1) {
                $startOccurrence = $parsedDate->addMonths($monthFreq * $index);
            }

            // Occurrence
            $startOccurrence = $parsedDate;

            // Payment Item Name
            $monthName       = $this->monthSorter(substr($startOccurrence, 5, 2));
            $paymentName     = $paymentItem->payment_item_name . " Bulan " . $monthName . " TA " . $epActive->id;

            return [
                'occurence' => $startOccurrence,
                'paymentName' => $paymentName
            ];
        }
    }

    private function monthSorter ($index)
    {
        return Date::createFromDate(0, $index, 10)->format('F');
    }
}
