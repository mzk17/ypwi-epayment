<?php

namespace App\Repositories\Contracts;

interface RepositoryInterface
{
    public function all ();

    public function paginate ($perPage = 15, $columns = array('*'));

    public function find ($id);

    public function findBy ($field, $value, $columns = array('*'));

    public function create (array $payload);

    public function update (array $payload);

    public function delete ($id);

    public function collectBy ($collection, $columns = array('*'));

    public function groupedBy ($collection, $identifier);
}
