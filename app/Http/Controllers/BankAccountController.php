<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\BankAccountResource;
use App\Repositories\BankAccountRepository as BankAccount;

class BankAccountController extends Controller
{
    protected $bankAccount;

    public function __construct(BankAccount $bankAccount)
    {
        $this->bankAccount = $bankAccount;
    }

    public function index ()
    {
        return response()->json(
            BankAccountResource::collection($this->bankAccount->all())
        );
    }

    public function view ($id)
    {
        return response()->json(
            $this->bankAccount->findBy('account_number', $id)
        );
    }

    public function schoolView (Request $request)
    {
        return response()->json(
            new BankAccountResource(
                $this->bankAccount->findBy('school_id', $request->school_id)
            )
        );
    }

    public function store (Request $request)
    {
        return response()->json(
            $this->bankAccount->store($request)
        );
    }

    public function update (Request $request)
    {
        return response()->json(
            $this->bankAccount->edit($request)
        );
    }

    public function delete (Request $request)
    {
        return response()->json(
            $this->bankAccount->deleteEntity($request)
        );
    }
}
