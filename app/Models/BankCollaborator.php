<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankCollaborator extends Model
{
    public $incrementing = false;
    protected $guarded = [];

    public function onSchools ()
    {
        return $this->hasMany('App\Models\BankAccount', 'bank_id');
    }
}
