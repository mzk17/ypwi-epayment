<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BankCollaborator;

class BankCollaboratorRepository extends BankCollaborator
{
    public function model ()
    {
        return 'App\Models\BankCollaborator';
    }
}
