<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Events\BaseCostCreated;
use App\Http\Resources\PaymentItemExchequerResource;
use App\Repositories\PaymentItemRepository as PaymentItem;

class PaymentItemController extends Controller
{
    private $paymentItem;

    public function __construct (PaymentItem $paymentItem)
    {
        $this->paymentItem = $paymentItem;
    }

    public function index ()
    {
        return response()->json(
            $this->paymentItem->all()
        );
    }

    public function store (Request $request)
    {
        return response()->json(
            $this->paymentItem->store($request)
        );
    }

    public function update (Request $request, $itemId)
    {
        return response()->json(
            $this->paymentItem->edit($itemId, $request)
        );
    }

    public function destroy ($itemId)
    {
        return response()->json(
            $this->paymentItem->delete($itemId)
        );
    }

    /**
     * Base Cost
     */
    public function ecqIndex ($schoolId)
    {
        $paymentItems = \App\Models\PaymentItem::with(['baseCost' => function ($query) use ($schoolId) {
                $query->where('school_id', $schoolId);
            },
            'timing'])
            ->select(['id', 'payment_item_name', 'timing_id', 'flag_class_level'])
            ->get();
        return response()->json(
            PaymentItemExchequerResource::collection($paymentItems)
        );
    }

    public function updateBaseCost (Request $request)
    {
        $piBase = \App\Models\PaymentItemBaseCost::where('payment_item_id', $request->payment_item_id)
            ->where('school_id', $request->school_id)
            ->where('period_id', $request->active_ep)
            ->update([
                'cost' => $request->cost
            ]);

        $payload = [
            'payment_item_id' => $request->payment_item_id,
            'school_id' => $request->school_id,
            'cost' => $request->cost
        ];

        event(new BaseCostCreated($payload));

        return response()->json('Base Cost of Payment Item has been updated successfully');
    }
}
