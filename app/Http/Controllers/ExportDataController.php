<?php

namespace App\Http\Controllers;

use App\Exports\TransactionsExport;
use App\Exports\InvoicesExport;
use Illuminate\Http\Request;

class ExportDataController extends Controller
{
    public function transactions()
    {
        return (new TransactionsExport)->download('trx.xlsx');
    }

    public function invoices()
    {
        return (new InvoicesExport)->download('invoices.xlsx');
    }
}
