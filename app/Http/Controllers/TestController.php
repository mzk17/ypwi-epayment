<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

use Uuid;
use DB;
use App\Models\ActivePayment;
use App\Models\PaymentItem;
use App\Models\StudentPaymentItem;
use App\Helpers\CurlVariables;

class TestController extends Controller
{
    use CurlVariables;

    /**
     * Link Active Payment to Students
     */
    public function linkActivePaymentToStudents (Request $request)
    {
        if ($request->activePaymentCreated) {
            // Init
            $eligibleStudents = [];

            // Active Payments
            $activePayments = ActivePayment::where('status', true)->get();

            // Students data on each Schools mapping by Class Level from MISS
            $uri        = config('API_URL_MISS').'school-class/all-students-in-schools';
            $studentMap = Curl::to($uri)
                            ->withData(array('epy' => 'lkActPyToStd'))
                            ->asJson()
                            ->get();

            foreach ($studentMap as $schoolId => $studentMapOnClass) {
                if (!empty($studentMapOnClass)) {
                    $eligibleStudents = array_merge($eligibleStudents, $this->linkPaymentToStudent($schoolId, $studentMapOnClass, $activePayments));
                }
            }

            try {
                DB::table('student_payment_items')->insert($eligibleStudents);
                return response()->json('Nice timing');
            }
            catch (Exception $e) {
                throw new Error($e->getMessage());
            }

        }
    }

    private function linkPaymentToStudent ($schoolId, $studentMap, $activePayments)
    {
        $eligibleStudents = [];

        foreach ($activePayments as $apk => $activePayment) {
            $mapKey = $activePayment->paymentItem->flag_class_level;

            if (array_key_exists($mapKey, $studentMap)) {
                // Link special Payment such as for students at first or last class level
                $eligibleStudents = array_merge($eligibleStudents, $this->linkClassLevel($schoolId, $studentMap->$mapKey, $activePayment));
            }
            else {
                foreach ($studentMap as $smk => $students) {
                    $eligibleStudents = array_merge($eligibleStudents, $this->linkClassLevel($schoolId, $students, $activePayment));
                }
            }
        }

        return $eligibleStudents;
    }

    private function linkClassLevel ($schoolId, $students, $activePayment)
    {
        $eligibleStudents = [];
        $baseCost = $activePayment->paymentItem->baseCost->where('school_id', $schoolId)->first();

        foreach ($students as $sk => $student) {
            array_push($eligibleStudents, [
                'id' => Uuid::generate(3, $student.$activePayment->id, Uuid::NS_DNS),
                'nis' => $student,
                'active_payment_id' => $activePayment->id,
                'cost' => $baseCost->cost,
                'status' => 0
            ]);
        }

        return $eligibleStudents;
    }

    public function cpibc ()
    {
        try {
            // Init
            $payload  = [];
            $schUri   = $this->curlVariables('getAllSchool');
            $schCurl  = Curl::to($schUri)->asJson()->get();
            $schools  = collect($schCurl)->pluck(['id']);

            $epActive = Curl::to($this->curlVariables('activeEducationPeriod'))->asJson()->get();

            $pis      = \App\Models\PaymentItem::select('id')->get();

            foreach ($schools as $key => $schoolId) {
                foreach ($pis as $pKey => $pi) {
                    array_push($payload, [
                        'payment_item_id' => $pi->id,
                        'school_id' => $schoolId,
                        'period_id' => $epActive->id,
                        'cost' => 0
                    ]);
                }
            }

            // DB::table('payment_item_base_costs')->insert($payload);

            return response()->json($epActive->id);
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    public function allStudentsInSchool ($school_id) {
        // Init
        $stdCurl  = Curl::to($this->curlVariables('getAllStudentsInAllSchools'))
            ->asJson()
            ->get();

        return response()->json($this->curlVariables('getAllStudentsInAllSchools'));
    }
}