<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Eloquent\GenericRepository;

abstract class PaymentTiming extends GenericRepository
{
    public function store ($payload)
    {
        try {
            $this->model->create($payload->all());

            return $this->responseMessage('Waktu Pembayaran berhasil dibuat');
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    public function edit ($timingId, $payload)
    {
        try {
            $this->model->where('id', $timingId)
                ->update($payload->all());
            
            return $this->responseMessage('Waktu Pembayaran berhasil diubah');
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    public function delete($timingId)
    {
        try {
            $this->model->find($timingId)->delete();
            return $this->responseMessage('Waktu Pembayaran berhasil dihapus');
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }
}
