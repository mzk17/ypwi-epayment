<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomBaseModel extends Model
{
    /**
     * This approach needs because we work with PouchDB
     * on frontend side.
     */
    protected $appends = ['_id'];

    public function getIdAttribute ()
    {
        return $this->attributes['_id'] = $this->updated_at;
    }
}