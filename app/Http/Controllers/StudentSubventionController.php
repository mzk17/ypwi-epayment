<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\StudentSubventionResource;
use App\Repositories\StudentSubventionRepository as StudentSubvention;

class StudentSubventionController extends Controller
{
    public $studentSubvention;

    public function __construct(StudentSubvention $studentSubvention)
    {
        $this->studentSubvention = $studentSubvention;
    }

    public function index ($nis)
    {
        return response()->json(
            StudentSubventionResource::collection($this->studentSubvention->index($nis))
        );
    }

    public function available ($nis)
    {
        return response()->json(
            $this->studentSubvention->available($nis)
        );
    }

    public function store (Request $request)
    {
        return response()->json(
            $this->studentSubvention->store($request)
        );
    }

    public function update (Request $request)
    {
        return response()->json(
            $this->studentSubvention->edit($request)
        );
    }

    public function delete(Request $request)
    {
        return response()->json(
            $this->studentSubvention->cancel($request)
        );
    }
}
