<?php

namespace App\Listeners;

use DB;
use App\Models\StudentPaymentItem;
use App\Models\PaymentItem;
use App\Events\BaseCostCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetupStudentPaymentBaseCost implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $activePI = $this->getActivePayments($event->payload['payment_item_id']);
        $students = $this->getAllStudents($event->payload['school_id']);

        try {
            StudentPaymentItem::whereIn('nis', $students)
                ->whereIn('active_payment_id', $activePI)
                ->update(['cost' => $event->payload['cost']]);

            return 'Student payment item base cost updated successfully';
        }
        catch (Exception $e) {
            throw new $e->getMessage();
        }
    }

    public function failed($event, $exception)
    {
        throw new $exception;
    }

    /**
     * Take all active payments ID of the updated base Payment Item
     * @return array of ID of Active Payment Items
     */
    private function getActivePayments ($paymentItemId)
    {
        $paymentItem = PaymentItem::find($paymentItemId);
        $activePI    = $paymentItem->activePayments->where('status', 1)->pluck('id');

        return $activePI;
    }

    /**
     * Get all students in school
     * @return array $students
     */
    private function getAllStudents ($schoolId)
    {
        $classes = DB::connection('ypwi_miss')->table('school_classes')
            ->select('id')
            ->where('school_id', $schoolId)
            ->pluck('id');

        $students = DB::connection('ypwi_miss')->table('class_students')
            ->select('nis')
            ->whereIn('class_id', $classes)
            ->pluck('nis');

        return $students;
    }
}
