<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentItem extends Model
{
    protected $guarded = [];

    public function timing ()
    {
        return $this->belongsTo('App\Models\PaymentTiming', 'timing_id');
    }

    public function activePayments ()
    {
        return $this->hasMany('App\Models\ActivePayment', 'payment_item_id');
    }

    public function baseCost ()
    {
        return $this->hasMany('App\Models\PaymentItemBaseCost', 'payment_item_id');
    }

    public function studentSubventions ()
    {
        return $this->hasMany('App\Models\StudentSubvention', 'payment_item_id');
    }

    // Auto event handlers
    protected static function boot ()
    {
        parent::boot();

        static::deleting(function ($item) {
            $item->activePayments()->delete();
            $item->baseCost()->delete();
        });
    }
}
