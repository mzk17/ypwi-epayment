<?php

namespace App\Listeners;

use App\Models\StudentPaymentItem;
use App\Models\StudentPaymentTransaction;
use App\Events\Broadcast\Administrator\CancelTransaction;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RevokeStudentPaymentStatus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CancelTransaction $event)
    {
        $stp = $event->payload['student_payment_id'];

        if (strpos($stp, ';')) {
            $studentPayments = explode(';', $stp);

            foreach ($studentPayments as $key => $studentPaymentId) {
                StudentPaymentItem::where('id', $studentPaymentId)
                    ->update(['transaction_status' => 0]);
            }
        }
        else {
            StudentPaymentItem::where('id', $stp)
                ->update(['transaction_status' => 0]);
        }
    }

    public function failed(CancelTransaction $event, $exception)
    {
        $message = "Event failed with this message: " . $exception;
        return response()->json($message);
    }
}
