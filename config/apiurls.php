<?php

return [
    'API_URL_AUTH_SERVER' => env('API_URL_AUTH_SERVER'),
    'API_URL_BANK_DATA' => env('API_URL_BANK_DATA'),
    'API_URL_MISS' => env('API_URL_MISS'),
];