<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_payments', function (Blueprint $table) {
            $table->increments('id');   // consider to use UUID
            $table->integer('payment_item_id');
            $table->string('active_payment_name');
            $table->boolean('status');
            $table->date('start_occurrence_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_payments');
    }
}
