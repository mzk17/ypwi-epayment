<?php

namespace App\Helpers;

trait CurlVariables
{
    public function curlVariables ($destination, $params = null)
    {
        switch ($destination) {
            case 'activeEducationPeriod':
                $baseUrl = config('apiurls.API_URL_MISS');
                $url = $baseUrl."education-period/active";
                break;
            case 'getAllSchool':
                $baseUrl = config('apiurls.API_URL_BANK_DATA');
                $url = $baseUrl."schools";
                break;
            case 'getAllStudentsInAllSchools':
                $baseUrl = config('apiurls.API_URL_MISS');
                $url = $baseUrl."school-class/all-students-in-schools";
                break;
            case 'getAllStudentsInSchool':
                $baseUrl = config('apiurls.API_URL_MISS');
                $url = $baseUrl."school-class/school/{$params['school_id']}/all-students";
                break;
            default:
                $baseUrl = "";
                $url = $baseUrl;
                break;
        }

        return $url;
    }
}