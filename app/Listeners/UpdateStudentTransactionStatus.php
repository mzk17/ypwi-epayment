<?php

namespace App\Listeners;

use App\Models\StudentPaymentItem;
use App\Events\StudentHaveCheckout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Helpers\Transaction\CalculateRemainingCost;

class UpdateStudentTransactionStatus implements ShouldQueue
{
    use CalculateRemainingCost;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(StudentHaveCheckout $event)
    {
        $checkoutPayment = $event->studentPaymentTransaction->student_payment_id;

        // Check if it was single or multiple transactions
        if (strpos($checkoutPayment, ';')) {
            $totalTransactions = explode(';', $checkoutPayment);
            foreach ($totalTransactions as $key => $studentPaymentId) {
                $this->updateTransactionStatus($studentPaymentId);
            }
        }
        else {
            $this->updateTransactionStatus($checkoutPayment);
        }
    }

    public function failed (StudentHaveCheckout $event, $exception)
    {
        $message = "Event failed with this message: " . $exception;
        return response()->json($message);
    }

    private function updateTransactionStatus ($studentPaymentId)
    {
        // Check the remainder of payment item that currently checked out
        $remainder = $this->remainingCost($studentPaymentId);

        // If the item has been paid, change the status
        if ($remainder == 0) {
            try {
                StudentPaymentItem::where('id', $studentPaymentId)
                    ->update(['transaction_status' => 1]);
            }
            catch (Exception $e) {
                throw new Error($e->getMessage());
            }
        }
    }
}
