<?php

namespace App\Events\Broadcast\Administrator;

use App\Models\StudentPaymentTransaction;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Http\Resources\AdministratorStudentTransactionResource;

class NewTransaction implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $studentPaymentTransaction;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(StudentPaymentTransaction $studentPaymentTransaction)
    {
        $this->studentPaymentTransaction = $studentPaymentTransaction;
    }

    public function broadcastAs()
    {
        return 'new.transaction';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('admin-transaction-dashboard');
    }

    public function broadcastWith()
    {
        return [
            'payload' => new AdministratorStudentTransactionResource($this->studentPaymentTransaction),
            'message' => 'Terdapat transaksi baru'
        ];
    }
}
