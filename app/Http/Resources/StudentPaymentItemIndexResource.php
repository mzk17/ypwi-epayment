<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentPaymentItemIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'student_payment_id' => $this->id,
            'payment_name' => $this->activePayment->active_payment_name,
            'cost' => $this->cost
        ];
    }
}
