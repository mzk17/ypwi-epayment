<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class StudentPaymentTransaction extends Model
{
    public $incrementing = false;
    protected $guarded = [];

    public function studentPaymentItem ()
    {
        return $this->belongsTo('App\Models\StudentPaymentItem', 'student_payment_id');
    }

    protected $appends = ['full_name', 'school_data'];

    public function getFullNameAttribute ()
    {
        $bio = DB::connection('ypwi_bank_data')->select("SELECT first_name, last_name FROM biographies WHERE personal_id = '{$this->nis}'");

        return $this->attributes['full_name'] = $bio[0]->first_name . " " . $bio[0]->last_name;
    }

    public function getSchoolDataAttribute ()
    {
        $studentClass = DB::connection('ypwi_miss')->select("
            SELECT id, class_name, school_id FROM school_classes WHERE id = 
                (SELECT class_id FROM class_students WHERE nis = '{$this->nis}')
        ");

        $schoolData = DB::connection('ypwi_bank_data')->select("
            SELECT school_name FROM schools WHERE id = '{$studentClass[0]->school_id}'
        ");

        $output['class_id']    = $studentClass[0]->id;
        $output['class_name']  = $studentClass[0]->class_name;
        $output['school_name'] = $schoolData[0]->school_name;

        return $this->attributes['school_data'] = $output;
    }
}
