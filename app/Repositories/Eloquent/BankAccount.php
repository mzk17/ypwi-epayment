<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Eloquent\GenericRepository;

abstract class BankAccount extends GenericRepository
{
    public function store ($payload)
    {
        try {
            $this->model->create($payload->all());

            return "Akun Bank berhasil ditambahkan";
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    public function edit ($payload)
    {
        $key = $payload->old_account_number;

        try {
            $newData = $payload->except([
                'old_account_number',
                'bank_name',
                'school_name'
            ]);

            $this->model->where('account_number', $key)
                ->update($newData);

            return "Akun bank {$payload->school_name} berhasil diubah";
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    public function deleteEntity ($payload)
    {
        try {
            $bankAccount = $this->model->find($payload->id);
            $bankAccount->delete();

            return "Akun bank berhasil dihapus";
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }
}