<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\BankAccount;

class BankAccountRepository extends BankAccount
{
    public function model ()
    {
        return 'App\Models\BankAccount';
    }
}
