<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_item_name');
            $table->integer('timing_id');
            $table->integer('timing_amount');
            $table->boolean('flag_class_first_level')->nullable();
            $table->boolean('flag_class_last_level')->nullable();
            $table->boolean('flag_manual')->nullable();
            $table->string('optional_slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_items');
    }
}
