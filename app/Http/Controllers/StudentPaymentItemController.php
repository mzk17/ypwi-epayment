<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\StudentPaymentItemIndexResource;
use App\Repositories\StudentPaymentItemRepository as StudentPaymentItem;

class StudentPaymentItemController extends Controller
{
    private $studentPaymentItem;

    public function __construct (StudentPaymentItem $studentPaymentItem)
    {
        $this->studentPaymentItem = $studentPaymentItem;
    }

    public function index ($nis)
    {
        return response()->json(
            StudentPaymentItemIndexResource::collection($this->studentPaymentItem->index($nis))
        );
    }
}