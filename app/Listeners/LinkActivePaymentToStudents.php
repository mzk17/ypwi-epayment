<?php

namespace App\Listeners;

use Ixudra\Curl\Facades\Curl;
use App\Models\ActivePayment;
use App\Helpers\CurlVariables;
use App\Events\NewEducationPeriodActivated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use DB;
use Uuid;

class LinkActivePaymentToStudents implements ShouldQueue
{
    use CurlVariables;

    private $activeEP;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->activeEP = DB::connection('ypwi_miss')->table('education_periods')
            ->select('id')
            ->where('status', 1)
            ->pluck('id')[0];
    }

    /**
     * Handle the event.
     *
     * @param  NewEducationPeriodActivated $event
     * @return void
     */
    public function handle(NewEducationPeriodActivated $event)
    {
        if ($event->creation_result) {
            // Init
            $eligibleStudents = [];

            // Get all Active Payments
            $activePayments = ActivePayment::where('status', true)->get();

            // Get Students data on each Schools mapping by Class Level from MISS
            $studentMap = Curl::to($this->curlVariables('getAllStudentsInAllSchools'))
                            ->withData(array('epy' => 'lkActPyToStd'))
                            ->asJson()
                            ->get();

            // Mapping. Generate eligible students in right format
            foreach ($studentMap as $schoolId => $studentMapOnClass) {

                if (!empty($studentMapOnClass)) {
                    $eligibleStudents = array_merge(
                        $eligibleStudents, 
                        $this->linkPaymentToStudent($schoolId, $studentMapOnClass, $activePayments)
                    );
                }

            }

            // Action
            try {
                DB::table('student_payment_items')->insert($eligibleStudents);
                return response()->json('Nice timing');
            }
            catch (Exception $e) {
                throw new Error($e->getMessage());
            }
        }
    }

    public function failed($event, $exception)
    {
        return $exception;
    }

    
    /**
     * Generate linked Active Payment & Students
     * @param string $schoolId
     * @param array $studentMap
     * @param array $activePayments
     */
    private function linkPaymentToStudent ($schoolId, $studentMap, $activePayments)
    {
        $eligibleStudents = [];

        foreach ($activePayments as $apk => $activePayment) {
            /**
             * Link special Payment
             * such as for students at first or last class level
             */
            $mapKey = $activePayment->paymentItem->flag_class_level;

            if (array_key_exists($mapKey, $studentMap)) {
                $eligibleStudents = array_merge(
                    $eligibleStudents, 
                    $this->linkClassLevel($schoolId, $studentMap->$mapKey, $activePayment)
                );
            }
            else {
                foreach ($studentMap as $smk => $students) {
                    $eligibleStudents = array_merge(
                        $eligibleStudents, 
                        $this->linkClassLevel($schoolId, $students, $activePayment)
                    );
                }
            }
        }

        return $eligibleStudents;
    }


    /**
     * Special function to determine special Active Payments that
     * should linked to Students by their Class level
     * @param string $schoolId
     * @param array $students
     * @param object $activePayment
     */
    private function linkClassLevel ($schoolId, $students, $activePayment)
    {
        $eligibleStudents = [];
        $baseCost = $activePayment->paymentItem->baseCost
            ->where('school_id', $schoolId)
            ->where('period_id', $this->activeEP)
            ->first();

        foreach ($students as $sk => $student) {
            array_push($eligibleStudents, [
                'id' => Uuid::generate(3, $student.$activePayment->id, Uuid::NS_DNS),
                'nis' => $student,
                'active_payment_id' => $activePayment->id,
                'cost' => $baseCost->cost,
                'transaction_status' => 0,
                'activity_status' => 1
            ]);
        }

        return $eligibleStudents;
    }
}
