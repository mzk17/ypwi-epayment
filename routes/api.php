<?php

use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Ixudra\Curl\Facades\Curl;

/**
 * Global Testing Purpose
 */
Route::get('/test-middleware', function () {
    return response()->json("Hurray");
})->middleware('check.jwt');

Route::get('/test-encode', function () {
    $key = 'syncrhonicity';
    $userId = '2720456909232190';

    return response()->json(
        JWT::encode($userId, $key)
    );
});

Route::get('/test-download', 'ExportDataController@transactions');
Route::get('/test-download-view', 'ExportDataController@invoices');

Route::prefix('v1/epy')->middleware('check.jwt')->group(function () {
    
    // Payment Timings
    Route::resource('payment-timing', 'PaymentTimingController')->only(['index', 'store', 'update', 'destroy']);
    
    // Payment Items
    Route::resource('payment-item', 'PaymentItemController')->only(['index', 'store', 'update', 'destroy']);
    
    // Base Cost (Payment Item)
    Route::prefix('payment-item/ecq')->group(function () {
        Route::get('{school_id}', 'PaymentItemController@ecqIndex');
        Route::put('update/base-cost', 'PaymentItemController@updateBaseCost');
    });

    // Education Period Activities
    Route::post('ex-action/education-period-created', 'ExternalHandlerController@educationPeriodCreated');
    Route::post('ex-action/education-period-cancel', 'ExternalHandlerController@educationPeriodCancel');

    // Active Payments
    Route::prefix('active-payment')->group(function () {
        Route::post('activation', 'ActivePaymentController@changeActivityWhenRunning');
        Route::get('/', 'ActivePaymentController@index');
    });

    // Student Payment Items
    Route::get('student-payment-items/{nis}', 'StudentPaymentItemController@index');
    
    // Student Subvention
    Route::prefix('student-subvention')->group(function () {
        Route::get('/{nis}', 'StudentSubventionController@index');
        Route::get('/{nis}/available', 'StudentSubventionController@available');
        Route::post('/', 'StudentSubventionController@store');
        Route::post('cancel', 'StudentSubventionController@delete');
        Route::put('update', 'StudentSubventionController@update');
    });

    // Student Transactions
    Route::prefix('student-payment-transaction')->group(function () {
        Route::post('checkout', 'StudentPaymentTransactionController@checkout');
        Route::post('archive', 'StudentPaymentTransactionController@archiveTransactions');
        Route::put('edit/{transaction_id}', 'StudentPaymentTransactionController@edit');
        Route::delete('cancel-checkout/{transaction_id}', 'StudentPaymentTransactionController@cancelCheckout');
        Route::match(['get', 'post'], 'all/{schoolId}', 'StudentPaymentTransactionController@index');
        Route::get('student/{nis}', 'StudentPaymentTransactionController@indexPerStudent');
        Route::get('remaining-cost/{student_payment_id}', 'StudentPaymentTransactionController@showRemainingCost');
        Route::get('view/{transaction_id}', 'StudentPaymentTransactionController@view');
        
        // Special for Administrator (Bendahara Pusat)
        Route::match(['get', 'post'], '/', 'StudentPaymentTransactionController@allTransactions');
        Route::post('/current-update', 'StudentPaymentTransactionController@allTransactionsCu');
    });
    
    // Bank Account for Schools
    Route::prefix('bank-account')->group(function () {
        Route::get('/', 'BankAccountController@index');
        Route::get('/{id}', 'BankAccountController@view');
        Route::post('/school', 'BankAccountController@schoolView');
        Route::post('/', 'BankAccountController@store');
        Route::post('/update', 'BankAccountController@update');
        Route::post('/delete', 'BankAccountController@delete');
    });

    // Bank Profile
    Route::prefix('bank-profile')->group(function () {
        Route::get('/', 'BankCollaboratorController@index');
        Route::post('/', 'BankCollaboratorController@store');
        Route::post('/update', 'BankCollaboratorController@update');
        Route::post('/delete', 'BankCollaboratorController@delete');
    });

    // Testing Purpose
    Route::prefix('test')->group(function () {
        Route::post('link-active-payment-to-students', 'TestController@linkActivePaymentToStudents')
            ->name('lapts');
        Route::post('create-payment-item-base-cost', 'TestController@cpibc');
        Route::get('coba-asis/{school_id}', 'TestController@allStudentsInSchool');
    });

});
