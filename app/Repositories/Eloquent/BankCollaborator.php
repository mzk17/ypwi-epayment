<?php

namespace App\Repositories\Eloquent;

use Uuid;
use App\Repositories\Eloquent\GenericRepository;

abstract class BankCollaborator extends GenericRepository
{
    public function store ($payload)
    {
        $data = $payload->all();
        $data['id'] = Uuid::generate(4);

        try {
            $this->model->create($data);

            return "Profil Bank berhasil ditambahkan";
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    public function edit ($payload)
    {
        $id = $payload->id;

        try {
            $this->model->where('id', $id)
                ->update([
                    'bank_name' => $payload->bank_name,
                    'bank_logo' => $payload->bank_logo
                ]);

            return 'Profil Bank berhasil diubah';
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    public function deleteEntity ($payload)
    {
        try {
            $bankProfile = $this->model->where('id', $payload->id);
            $bankProfile->delete();

            return 'Profil Bank berhasil dihapus';
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }
}