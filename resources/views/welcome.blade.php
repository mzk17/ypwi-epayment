<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Nama Item</th>
        </tr>
    </thead>
    <tbody>
    @foreach($payment_items as $pi)
        <tr>
            <td>{{ $pi->id }}</td>
            <td>{{ $pi->payment_item_name }}</td>
        </tr>
    @endforeach
    </tbody>
</table>