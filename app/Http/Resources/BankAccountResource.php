<?php

namespace App\Http\Resources;

use DB;
use Illuminate\Http\Resources\Json\JsonResource;

class BankAccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'school_id' => $this->school_id,
            'bank_id' => $this->bank_id,
            'school_name' => $this->schoolName($this->school_id),
            'bank_name' => $this->bankProfile->bank_name,
            'bank_logo' => $this->bankProfile->bank_logo,
            'account_number' => $this->account_number,
            'account_type' => $this->account_type,
            'account_name' => $this->account_name
        ];
    }

    private function schoolName ($schoolId)
    {
        $school = DB::connection('ypwi_bank_data')->table('schools')
            ->select('school_name')
            ->where('id', $schoolId)
            ->get()[0];
        
        return $school->school_name;
    }
}
