<?php

namespace App\Exports;

use App\Models\PaymentItem;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class InvoicesExport implements FromView, ShouldAutoSize
{
    use Exportable;

    public function view(): View
    {
        return view('welcome', [
            'invoices' => PaymentItem::all()
        ]);
    }
}