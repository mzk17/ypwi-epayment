<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ActivePaymentRepository as ActivePayment;

class ActivePaymentController extends Controller
{
    private $activePayment;

    public function __construct (ActivePayment $activePayment)
    {
        $this->activePayment = $activePayment;
    }

    public function index ()
    {
        return response()->json(
            $this->activePayment->index()
        );
    }

    public function educationPeriodCreated (Request $request)
    {
        return response()->json(
            $this->activePayment->generate($request)
        );
    }

    public function educationPeriodCancel (Request $request)
    {
        if ($request->cancel_ep) {
            return response()->json(
                $this->activePayment->epCanceled($request)
            );
        }
    }

    /**
     * The Idea
     * If there suddenly some reason to exclude or deactivate
     * the payment item, then it should not to be removed
     * Instead, it disabled to keep consistency of app.
     * `activity_status` on StudentPaymentItem will be 2
     * 
     * The means of Running
     * It should be "Active" payment(s) on current Education Period
     * that want to excluded
     * 
     * Payload properties
     * status => 1 or 2,
     * active_payment_id,
     * agree_to_change_activity_status => boolean
     */
    public function changeActivityWhenRunning (Request $request)
    {
        return response()->json(
            $this->activePayment->activation($request)
        );
    }
}
