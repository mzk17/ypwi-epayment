<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivePayment extends Model
{
    protected $guarded = [];

    public function paymentItem ()
    {
        return $this->belongsTo('App\Models\PaymentItem', 'payment_item_id');
    }

    public function studentPaymentItem ()
    {
        return $this->hasMany('App\Models\StudentPaymentItem', 'active_payment_id');
    }

    public static function classLevelPayment ($level)
    {
        return static::join('payment_items',
            'payment_items.id', '=', 'active_payments.payment_item_id')
        ->where('payment_items.flag_class_level', $level)
        ->where('active_payments.status', true)
        ->get();
    }
}
