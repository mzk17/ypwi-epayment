<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdministratorStudentTransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $checkoutPayment = $this->student_payment_id;

        // Check if it was single or multiple transactions
        if (strpos($checkoutPayment, ';')) {
            $totalTransactions = explode(';', $checkoutPayment);
            $paymentName = count($totalTransactions) . " item pembiayaan";
            $paymentStatus = 'Detail';
        }
        else {
            $paymentName = $this->studentPaymentItem->activePayment->active_payment_name;
            $paymentStatus = ($this->studentPaymentItem->transaction_status) ? 'Lunas' : 'Belum lunas';
        }

        return [
            'transaction_id' => $this->id,
            'payment_name' => $paymentName,
            'checkout' => $this->checkout,
            'note' => $this->note,
            'status' => $paymentStatus,
            'student_data' => [
                'nis' => $this->nis,
                'full_name' => $this->full_name,
                'school_name' => $this->school_data['school_name'],
                'class_name' => $this->school_data['class_name'],
                'class_id' => $this->school_data['class_id']
            ],
            'transaction_date' => $this->updated_at
        ];
    }
}
