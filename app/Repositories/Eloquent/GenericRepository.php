<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;

abstract class GenericRepository implements RepositoryInterface
{
    private $app;

    protected $model;

    public function __construct (App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    abstract function model ();

    public function makeModel ()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model)
            dd('Error! The object is not a Model instance');

        return $this->model = $model;
    }

    /**
     *  Contract methods (Eloquent)
     */
    public function all()
    {
        return $this->model->all();
    }

    public function skipTake($skip)
    {
        return $this->model->skip($skip)->take(100)->get();
    }

    public function count()
    {
        return $this->model->count();
    }

    public function paginate ($perPage = 15, $columns = array('*'))
    {
        return $this->model->paginate($perPage, $columns);
    }

    public function find ($id)
    {
        return $this->model->find($id);
    }

    public function findBy ($field, $value, $columns = array('*'))
    {
        return $this->model->where($field, $value)->first($columns);
    }

    public function create (array $payload)
    {
        return $this->model->create($payload);
    }

    public function update (array $payload)
    {
        return $this->model->update($payload);
    }

    public function delete ($id)
    {
        $entity = $this->find($id);
        try {
            $entity->delete();
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }

    public function destroy (array $arrayOfId)
    {
        return $this->model->destroy($arrayOfId);
    }

    /**
     *  Custom methods (Non-eloquent)
     */

    // Return collection by specific columns
    public function collectBy ($collection, $columns = array('*'))
    {
        $newCollection = $collection->map(function ($entity, $key) use ($columns) {
            return collect(
                collect($entity)->only($columns)
            );
        });

        return $newCollection;
    }

    public function groupedBy ($collection, $identifier)
    {
        $collection = collect($collection->toArray());
        $newCollection = $collection->map(function ($entity, $key) use ($identifier) {
            return $this->model->find($entity[$identifier]);
        });

        return $newCollection;
    }

    public function responseMessage ($message = 'Response message', $payload = null)
    {
        return [
            'message' => $message,
            'payload' => $payload
        ];
    }
}
