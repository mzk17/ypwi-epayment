<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\NewEducationPeriodActivated' => [
            'App\Listeners\CreatePaymentItemBaseCost',
            'App\Listeners\LinkActivePaymentToStudents',
        ],
        'App\Events\BaseCostCreated' => [
            'App\Listeners\SetupStudentPaymentBaseCost',
            'App\Listeners\SetupStudentPaymentSubvention',
        ],
        'App\Events\StudentHaveCheckout' => [
            'App\Listeners\UpdateStudentTransactionStatus'
        ],
        'App\Events\Broadcast\Administrator\CancelTransaction' => [
            'App\Listeners\RevokeStudentPaymentStatus'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
