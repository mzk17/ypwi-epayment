<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Eloquent\GenericRepository;

abstract class StudentPaymentItem extends GenericRepository
{
    public function index ($nis)
    {
        return $this->model->where('nis', $nis)
            ->where('transaction_status', 0)
            ->with('activePayment')
            ->select(['id', 'cost', 'active_payment_id'])
            ->get();
    }
}