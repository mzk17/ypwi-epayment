<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ActivePaymentRepository as ActivePayment;

class ExternalHandlerController extends Controller
{
    private $activePayment;

    public function __construct(ActivePayment $activePayment)
    {
        $this->activePayment = $activePayment;
    }

    public function educationPeriodCreated (Request $request)
    {
        return response()->json(
            $this->activePayment->generate($request)
        );
    }

    public function educationPeriodCancel (Request $request)
    {
        if ($request->cancel_ep) {
            return response()->json(
                $this->activePayment->epCanceled($request)
            );
        }
    }
}
