<?php

namespace App\Repositories\Eloquent;

use DB;
use Uuid;

use Carbon\Carbon;
use Ixudra\Curl\Facades\Curl;

use App\Helpers\CurlVariables;
use App\Http\Resources\AdministratorStudentTransactionResource;

use App\Events\StudentHaveCheckout;
use App\Events\Broadcast\Administrator\NewTransaction;
use App\Events\Broadcast\Administrator\CancelTransaction;

use App\Models\ActivePayment;
use App\Models\StudentPaymentItem;
use App\Repositories\Eloquent\GenericRepository;

abstract class StudentPaymentTransaction extends GenericRepository
{
    use CurlVariables;

    /**
     * Show all transactions in a Student
     */
    public function indexPerStudent ($nis)
    {
        return $this->model->where('nis', $nis)->get();
    }


    /**
     * Show all transactions in a School
     */
    public function index ($schoolId, $payload)
    {
        // Fetch all students in the school or in the all schools
        $students = $this->allStudentsInSchool($schoolId);

        if (!empty($students)) {
            // Fetch the transactions of that students
            $result = $this->model->whereIn('nis', collect($students)->pluck('nis'))
                ->skip($payload->skip)
                ->take($payload->take)
                ->orderBy('updated_at', 'DESC')
                ->get();
        }
        else {
            $result = [];
        }

        return $result;
    }

    private function allStudentsInSchool ($schoolId)
    {
        // Query to other DB
        $students = DB::connection('ypwi_miss')->select("
            SELECT nis FROM class_students WHERE class_id IN (SELECT id FROM school_classes WHERE school_id = '{$schoolId}')
        ");

        return $students;
    }

    /**
     * All transactions for Administrator
     * Central / Lead of Exchequer
     */
    public function adminTransactions($skip = 0, $take = 25)
    {
        $result = $this->model->skip($skip)
            ->take($take)
            ->orderBy('updated_at', 'DESC')
            ->get();

        return $result;
    }

    public function adminTransactionsCurrentUpdates ($lastEntity)
    {
        $formatted = Carbon::parse($lastEntity);
        $result = $this->model->where('created_at', '>', $formatted)
            ->orderBy('updated_at', 'DESC')
            ->get();
        
        return $result;
    }


    /**
     * Checkout transaction
     */
    public function store ($payload)
    {
        // If checkout amount is bigger than remaining cost, return warning and stop execute
        if (!$this->checkIncomingTransactionAmount($payload)) {
            return [
                'status' => 101,
                'message' => 'Transaksi tidak dapat dilanjutkan. Anda memasukkan nominal yang berlebihan'
            ];
        }

        // Generate UUID and push into transaction array
        $idGenerator       = $payload->nis . '-' . date("hisa");
        $idTransaction     = Uuid::generate(3, $idGenerator, Uuid::NS_DNS);
        $transaction['id'] = $idTransaction->string;

        // Push other data to transaction array
        foreach ($payload->all() as $key => $value)
            $transaction[$key] = $value;

        try {
            // Action
            $newTransaction = $this->model->create($transaction);

            /**
             * Update status of Student Payment if it has been paid
             */
            event(new StudentHaveCheckout($newTransaction));

            /**
             * Fire update student transaction status on this payment
             * and for the future, it should broadcast to Foundation
             * as realtime notification
             */
            broadcast(new NewTransaction($newTransaction));

            return $this->responseMessage('Transaksi berhasil dilakukan', new AdministratorStudentTransactionResource($newTransaction));
        }
        catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    private function checkIncomingTransactionAmount ($payload)
    {
        $amount = 0;
        $studentPaymentId = $payload->student_payment_id;

        if (strpos($studentPaymentId, ';')) {
            $totalTransactions = explode(';', $studentPaymentId);
            foreach($totalTransactions as $key => $studentPaymentId) {
                $studentPayment = StudentPaymentItem::find($studentPaymentId);
                $amount += $studentPayment->cost;
            }
        }
        else {
            $studentPayment = StudentPaymentItem::find($studentPaymentId);
            $amount = $studentPayment->cost;
        }

        return ($payload->checkout > $amount) ? false : true;
    }


    /**
     * Edit a transaction
     */
    public function edit ($transactionId, $payload)
    {
        try {
            // Fetch the entity
            $transaction = $this->model->find($transactionId);
            
            // Update values
            $transaction->checkout = (!empty($payload->checkout) || !is_null($payload->checkout)) ? $payload->checkout : $transaction->checkout;
            $transaction->note     = (!empty($payload->note) || !is_null($payload->note)) ? $payload->note : $transaction->note;
            
            // Action
            $transaction->save();

            return $this->responseMessage('Transaksi berhasil di-update');
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }


    /**
     * Cancel a checkout
     */
    public function delete ($transactionId)
    {
        try {
            $transaction = $this->model->find($transactionId);

            $payload = [
                'transaction_id' => $transaction->id,
                'student_payment_id' => $transaction->student_payment_id
            ];

            /**
             * Revoke the student payment item's transaction status
             * and then, delete it
             */
            broadcast(new CancelTransaction($payload));

            $transaction->delete();

            return $this->responseMessage('Transaksi berhasil dibatalkan');
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }
}