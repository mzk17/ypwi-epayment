<?php

namespace App\Helpers\ActivePayment;

use DB;

use Ixudra\Curl\Facades\Curl;
use App\Models\PaymentItem;
use App\Repositories\Eloquent\GenericRepository;
use App\Helpers\CurlVariables;
use App\Helpers\ActivePayment\OccurenceAndNameGenerator;
use App\Events\ActivePaymentCreated;
use App\Events\NewEducationPeriodActivated;

abstract class ActivePaymentGenerator extends GenericRepository
{
    use OccurenceAndNameGenerator, CurlVariables;

    /** 
     * Deactivate any previous active payments at previous education period
     */
    public function deactivatePreviousPayments ()
    {
        $this->model->where('status', true)
            ->update(['status' => false]);
    }

    /** 
     * Activate new Payments based on currently activated education period
     */
    public function createActivePayments ($payload)
    {
        // Init. Get the active Education Period
        $epActive = DB::connection('ypwi_miss')->table('education_periods')
            ->select('id')
            ->where('status', 1)
            ->get()[0];

        try {
            // Generate active payments
            $this->oncePayment($epActive);
            $this->routinePayment($epActive, $payload);

            /**
             * 1. Create base cost of each payment item
             *    every new period activated.
             * 2. This will be link the Active Payments to proper students.
             */
            event (new NewEducationPeriodActivated(['creation_result' => true]));
            
            return true;
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    /**
     * @var RoutinePayments
     * Create active payments for any Routine items
     * belongs to current education period
     */
    private function routinePayment ($epActive, $reqStartTime)
    {
        // Get all routine type of Payment Items
        $paymentItems = PaymentItem::where('timing_id', '>', 1)->get();

        try {
            // Create Active Payment Items that will have total items based on it's timing
            foreach ($paymentItems as $key => $paymentItem) {

                $totalPayment = $this->unitDivider($paymentItem);
                $rst          = "pid_".$paymentItem->id."_sf";

                // Action
                for ($i = 1; $i <= $totalPayment; $i++)
                    $this->store('routine', $paymentItem, $epActive, $i, $reqStartTime->{$rst});

            }

            return true;
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    /**
     * @var OnceTimePayments
     * Create active payments for any One LifeTime items
     * belongs to current education period
     */
    private function oncePayment ($epActive)
    {
        $paymentItems = PaymentItem::where('timing_id', 1)->get();

        try {

            // Action
            foreach ($paymentItems as $key => $paymentItem)
                $this->store('once', $paymentItem, $epActive);
    
            return true;
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    /**
     * Main task for create those ActivePayments
     */
    private function store (string $timingType, $paymentItem, $epActive, $index = null, $reqStartTime = null)
    {
        $activePayment = new $this->model;
                
        // ID of Payment Item parent
        $activePayment->payment_item_id = $paymentItem->id;

        // Create Active Payment Name & Occurence time
        $pnoGenerator                         = $this->generator($index, $paymentItem, $reqStartTime, $epActive);
        $activePayment->active_payment_name   = ($timingType === 'routine') ? $pnoGenerator['paymentName'] : $paymentItem->payment_item_name . " TA " . $epActive->id;
        $activePayment->start_occurrence_time = ($timingType === 'routine') ? $pnoGenerator['occurence'] : null;

        // Status
        $activePayment->status = true;

        $activePayment->save();
    }

    private function unitDivider ($item)
    {
        switch ($item->timing->timing_unit) {
            case 'm':
                $output = 12/$item->timing_amount;
                break;
            
            default:
                $output = $item->timing_amount;
                break;
        }

        return $output;
    }
}
