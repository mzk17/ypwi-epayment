<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\PaymentItem;

class PaymentItemRepository extends PaymentItem
{
    public function model ()
    {
        return 'App\Models\PaymentItem';
    }
}
