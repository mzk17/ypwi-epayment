<?php

namespace App\Events\Broadcast\Administrator;

use App\Models\StudentPaymentTransaction;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CancelTransaction implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $payload;

    /**
     * Create a new event instance.
     *
     * @return $payload
     * ['transaction_id', 'student_payment_id']
     */
    public function __construct($payload)
    {
        $this->payload = $payload;
    }

    public function broadcastAs()
    {
        return 'cancel.transaction';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('admin-transaction-dashboard');
    }

    public function broadcastWith()
    {
        return [
            'payload' => $this->payload['transaction_id'],
            'message' => 'Terdapat transaksi yang dibatalkan'
        ];
    }
}
