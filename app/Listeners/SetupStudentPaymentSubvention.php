<?php

namespace App\Listeners;

use App\Models\ActivePayment;
use App\Models\StudentSubvention;
use App\Models\StudentPaymentItem;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SetupStudentPaymentSubvention implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // Take list of subvention students
        $subvStudents = StudentSubvention::select(['nis', 'subvention_cost'])
            ->where('payment_item_id', $event->payload['payment_item_id'])
            ->get();

        // Take the Active Payemnts based on that BasePayment
        $activePayments = ActivePayment::select('id')
            ->where('payment_item_id', $event->payload['payment_item_id'])
            ->where('status', 1)
            ->pluck('id');

        // Update them according to their subvention
        foreach ($subvStudents as $key => $subvStudent) {
            StudentPaymentItem::where('nis', $subvStudent->nis)
                ->whereIn('active_payment_id', $activePayments)
                ->update(['cost' => $subvStudent->subvention_cost]);
        }
    }
}
