<?php

namespace App\Http\Middleware;

use DB;
use Closure;
use Firebase\JWT\JWT;

class CheckJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->header('Authorization');

        if (is_null($header) || empty($header)) {
            return response()->json(['Unauthorized'], 401);
        }
        else if (!$this->checkToken($header)) {
            return response()->json(['Unauthorized'], 401);
        }

        return $next($request);
    }

    private function checkToken($header)
    {
        // Check token type
        $tokenType = explode(" ", $header)[0];

        if ($tokenType != "Bearer")
            return false;

        // If it comes from right Auth Server / Issuer
        $key = 'krH5PKb8DVvOiGLreFzFG9X8dKMnfdjW';
        $decoded = JWT::decode(
            explode(" ", $header)[1], $key, array('HS256')
        );

        if ($this->checkIssuer($decoded->iss) && $this->checkUser($decoded->uid)) {
            return true;
        }

        return false;
    }

    private function checkIssuer($issuer)
    {
        $totalChar = strlen(config('API_URL_AUTH_SERVER'));
        $issuer = substr($issuer, 0, $totalChar);

        if ($issuer != config('API_URL_AUTH_SERVER'))
            return false;

        return true;
    }

    private function checkUser($userId)
    {
        $realId = base64_decode($userId);

        $user = DB::connection('ypwi_auth')->table('users')
            ->select('id')
            ->where('id', $realId)
            ->get();
        
        if (count($user) == 0)
            return false;

        return true;
    }
}
