<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyClassLevelFlag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_items', function (Blueprint $table) {
            $table->dropColumn(['flag_class_first_level', 'flag_class_last_level']);

            $table->string('flag_class_level', 6)
                ->after('timing_amount')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_items', function (Blueprint $table) {
            $table->boolean('flag_class_last_level')->after('timing_amount');
            $table->boolean('flag_class_first_level')->after('timing_amount');
            $table->dropColumn('flag_class_level');
        });
    }
}
