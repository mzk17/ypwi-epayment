<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExchequerStudentTransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $checkoutPayment = $this->student_payment_id;

        if (strpos($checkoutPayment, ';')) {
            $totalTransactions = explode(';', $checkoutPayment);
            $paymentName = count($totalTransactions) . " item pembiayaan";
        }
        else {
            $paymentName = $this->studentPaymentItem->activePayment->active_payment_name;
        }

        return [
            'transaction_id' => $this->id,
            'payment_name' => $paymentName,
            'checkout' => $this->checkout,
            'note' => $this->note,
            'student_data' => [
                'nis' => $this->nis,
                'full_name' => $this->full_name,
                'class_name' => $this->school_data['class_name']
            ],
            'transaction_date' => $this->updated_at
        ];
    }
}
