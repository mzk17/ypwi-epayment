<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentTiming extends Model
{
    protected $guarded = [];

    public function item ()
    {
        return $this->hasMany('App\Models\PaymentItem', 'timing_id');
    }
}
