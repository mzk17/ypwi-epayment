<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Eloquent\GenericRepository;
// use App\Events\PaymentItemCreated;

abstract class PaymentItem extends GenericRepository
{
    public function store ($payload)
    {
        try {
            $paymentItem = $this->model->create($payload->all());

            // Fire event that will be create Base Cost for each item created
            // event(new PaymentItemCreated($paymentItem));

            return $this->responseMessage('Item Pembayaran berhasil dibuat');
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    public function edit($id, $payload)
    {
        try {
            $this->model->where('id', $id)
                ->update($payload->all());

            return $this->responseMessage('Item Pembayaran berhasil diperbarui');
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $this->model->find($id)->delete();
            return $this->responseMessage('Item Pembayaran berhasil dihapus');
        }
        catch (Exception $e) {
            throw new Error($e->getMessage());
        }
    }
}
