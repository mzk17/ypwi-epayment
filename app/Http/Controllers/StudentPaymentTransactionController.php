<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Transaction\CalculateRemainingCost;
use App\Helpers\Archiving;
use App\Repositories\StudentPaymentTransactionRepository as StudentPaymentTransaction;
use App\Http\Resources\AdministratorStudentTransactionResource;
use App\Http\Resources\ExchequerStudentTransactionResource;

class StudentPaymentTransactionController extends Controller
{
    use CalculateRemainingCost, Archiving;

    private $studentPaymentTransaction;

    public function __construct (StudentPaymentTransaction $studentPaymentTransaction)
    {
        $this->studentPaymentTransaction = $studentPaymentTransaction;
    }


    /**
     * List all transaction by a Student
     */
    public function indexPerStudent ($nis)
    {
        return response()->json(
            AdministratorStudentTransactionResource::collection($this->studentPaymentTransaction->indexPerStudent($nis))
        );
    }


    /**
     * List all transaction by a School
     * Used on dashboard or timeline
     */        
    public function index (Request $payload, $schoolId)
    {
        $transactions = $this->studentPaymentTransaction->index($schoolId, $payload);

        return response()->json([
            'totalRows' => $this->studentPaymentTransaction->count(),
            'collection' => ExchequerStudentTransactionResource::collection($transactions)
        ]);
    }

    /**
     * List all transactions on all schools
     * Used on Administrator (Bendahara Pusat) dashboard
     */
    public function allTransactions (Request $payload)
    {
        $allTransactions = $this->studentPaymentTransaction->adminTransactions($payload->skip, $payload->take);

        return response()->json([
            'totalRows' => $this->studentPaymentTransaction->count(),
            'collection' => AdministratorStudentTransactionResource::collection($allTransactions)
        ]);
    }

    public function allTransactionsCu (Request $request)
    {
        $currentUpdates = $this->studentPaymentTransaction->adminTransactionsCurrentUpdates($request->lastEntity);

        return response()->json(
            AdministratorStudentTransactionResource::collection($currentUpdates)
        );
    }

    public function view ($transactionId)
    {
        return response()->json(
            new AdministratorStudentTransactionResource($this->studentPaymentTransaction->find($transactionId))
        );
    }


    /**
     * Global API that show remaining cost of a student paymeent item
     */
    public function showRemainingCost ($studentPaymentId)
    {
        return response()->json(
            ['remaining_cost' => $this->remainingCost($studentPaymentId)]
        );
    }


    /**
     * Main function of this service
     */
    public function checkout (Request $request)
    {
        return response()->json(
            $this->studentPaymentTransaction->store($request)
        );
    }


    /**
     * @var future
     * Archiving student transactions
     * When a student already graduate or out from school for any reason
     */
    public function archiveTransactions (Request $request)
    {}


    /**
     * Edit a transaction
     */
    public function edit ($transactionId, Request $request)
    {
        return response()->json(
            $this->studentPaymentTransaction->edit($transactionId, $request)
        );
    }


    /**
     * Cancel out a transaction
     */
    public function cancelCheckout ($transactionId)
    {
        return response()->json(
            $this->studentPaymentTransaction->delete($transactionId)
        );
    }
}
