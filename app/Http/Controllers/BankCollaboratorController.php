<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\BankCollaboratorRepository as BankCollaborator;

class BankCollaboratorController extends Controller
{
    protected $bankCollaborator;

    public function __construct(BankCollaborator $bankCollaborator)
    {
        $this->bankCollaborator = $bankCollaborator;
    }

    public function index ()
    {
        return response()->json(
            $this->bankCollaborator->all()
        );
    }

    public function store(Request $request)
    {
        return response()->json(
            $this->bankCollaborator->store($request)
        );
    }

    public function update(Request $request)
    {
        return response()->json(
            $this->bankCollaborator->edit($request)
        );
    }

    public function delete(Request $request)
    {
        return response()->json(
            $this->bankCollaborator->deleteEntity($request)
        );
    }
}
