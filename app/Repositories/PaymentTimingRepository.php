<?php

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\PaymentTiming;

class PaymentTimingRepository extends PaymentTiming
{
    public function model ()
    {
        return 'App\Models\PaymentTiming';
    }
}
