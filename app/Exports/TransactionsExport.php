<?php

namespace App\Exports;

// use DB;
use App\Models\StudentPaymentTransaction as Transaction;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
// use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TransactionsExport implements FromQuery, WithMapping, WithColumnFormatting, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function query()
    {
        return Transaction::query();
    }
    
    public function map($trx): array
    {
        return [
            $trx->nis,
            $trx->full_name,
            $trx->school_data['school_name'] . ", " . $trx->school_data['class_name'],
            $trx->studentPaymentItem->activePayment->active_payment_name,
            $trx->checkout,
            Date::dateTimeToExcel($trx->updated_at)
        ];
    }

    public function columnFormats(): array
    {
        return [
            'E' => 'Rp #,##0_-',
            'F' => 'mmm dd, yyyy',
        ];
    }

    public function headings(): array
    {
        return [
            'NIS',
            'Nama Siswa',
            'Sekolah',
            'Item Pembiayaan',
            'Jumlah Pembayaran',
            'Tanggal Pembayaran'
        ];
    }
}