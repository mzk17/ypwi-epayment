<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentItemExchequerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        switch ($this->flag_class_level) {
            case 'first':
                $fcl = 'Tingkatan pertama';
                break;
            case 'last':
                $fcl = 'Tingkatan terakhir';
                break;
            default:
                $fcl = 'Semua tingkatan';
                break;
        }

        return [
            'id' => $this->id,
            'payment_item_name' => $this->payment_item_name,
            'timing' => $this->timing->timing_name,
            'flag_class_level' => $fcl,
            'base_cost' => $this->baseCost[0]->cost
        ];
    }
}
